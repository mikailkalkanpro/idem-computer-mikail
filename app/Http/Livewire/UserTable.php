<?php
namespace App\Http\Livewire;

use App\Models\Role;
use App\Models\user;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class UserTable extends LivewireDatatable
{
    public $hideable = 'select';

    public function builder()
    {
        return User::query();
    }

    public function columns()
    {
        return [
            NumberColumn::name('id')->label('ID'),

            Column::name('name')->label('Nom')->searchable(),

            Column::name('email')->label('Email')->searchable(),

            Column::name('role.name')->label('Rôle')->unsortable(),

            Column::callback(['id'], function ($id) {
                return view('components.table-actions', ['id' => $id, 'routeName' => 'users']);
            })->unsortable()
        ];
    }
}
