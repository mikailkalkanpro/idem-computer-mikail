<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CrudController extends Controller
{
    public function index($modelName)
    {
        dd(__('Hello'));
        $model = 'App\Models\\' . ucfirst(Str::camel($modelName));
        return view('admin.crud.index', compact('model'));
    }
}
